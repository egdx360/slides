import React, { Component } from 'react';
import './App.css';
import Home from './Home';
import Slides from './Slides';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <BrowserRouter>
            <main className="App">
              <Switch>
                <Route path="/" exact component={Slides}/>
                <Route path="/test" component={Home} />
              </Switch>
            </main>
          </BrowserRouter>
        );
    }
}

export default App;
