import React from 'react';
import './Slides.css';
import Slideshow from 'react-slidez';

export default class Slides extends React.Component {
    constructor() {
        super();

        this.button = null;
        this.h1 = null;
        this.img = null;

        this.setButtonRef = element => {
            this.button = element;
        };

        this.setH1Ref = element => {
            this.h1 = element;
        };

        this.setImgRef = element => {
            this.img = element;
        };

        this.hide = () => {
            if (this.button) {
                this.button.style.display = 'none';
                this.h1.style.display = 'block';
                this.img.style.display = 'block';
            }
        };
    }

    render() {
        return (
            <Slideshow
                showIndex
                enableKeyboard
                useDotIndex
                defaultIndex={0}
                effect={'fade'}
                height={'100%'}
                width={'100%'}
                autoplay={false}
                showArrows={false}>
                <section id="one">
                    <div class="bys">
                        <img src="/logo.png" alt="logo" />
                        <h4>BOOST YOUR SKILLS</h4>
                    </div>
                    <h2 ref={this.setButtonRef}>
                        Welcome to the unplugged TSE BYS
                        <br />
                        <br />
                        We invite you to be fully present during this session.
                        <br />
                        <br />
                        Please put away your phone (and cameras, weapons or
                        choucroute (Sauerkraut))
                        <br />
                        and enjoy the moment.
                        <br />
                        <br />
                        <button onClick={this.hide}>#kindness</button>
                    </h2>
                    <h1 ref={this.setH1Ref}>CSS GRID</h1>
                    <img ref={this.setImgRef} src="/css.png" alt="main" />
                </section>

                <section id="two">
                    <h1>Summary :</h1>
                    <ul>
                        <li>
                            <strong>1. What is it ? 🤔</strong>
                        </li>
                        <li>
                            <strong>2. Why should we use it ? 💡</strong>
                        </li>
                        <li>
                            <strong>3. How about AB Tasty ? 🚀</strong>
                        </li>
                    </ul>
                </section>

                <section id="three" className="center">
                    <h1>1. What is it ?</h1>
                    <h1>
                        <img
                            class="emoji"
                            draggable="false"
                            alt="🤔"
                            src="https://twemoji.maxcdn.com/2/svg/1f914.svg"
                            data-marp-twemoji=""
                        />
                    </h1>
                </section>

                <section id="four">
                    <p>
                        The CSS Grid Layout Module offers a new grid-based
                        layout system, with rows and columns, making it easier
                        to design web pages
                        <br />
                        <strong>
                            without having to use floats, positioning or
                            frameworks 🎉
                        </strong>
                        <br />
                    </p>
                    <p>
                        There have been many other webpage layout methods used
                        previously including : tables <strong>(!)</strong>, the
                        box model, and CSS Flexbox.
                    </p>
                    <p>
                        However, for the first time ever, we now have{' '}
                        <strong>
                            a proper layout system available natively in all
                            major browsers
                        </strong>
                    </p>
                </section>

                <section id="five" className="center">
                    <img src="/example.png" />
                    <img src="/bootstrap.png" />
                </section>

                <section id="six">
                    <img src="/example.png" />
                    <img src="/uglyboot.png" />
                    <p>🤮🤮🤮</p>
                </section>

                <section id="seven">
                    <img src="/example.png" />
                    <img src="/semantic.png" />
                </section>

                <section id="eight">
                    <div class="wrapper">
                        <div class="header">HEADER</div>
                        <div class="menu">MENU</div>
                        <div class="content">CONTENT</div>
                        <div class="footer">FOOTER</div>
                    </div>
                </section>

                <section id="nine">
                    <h1>2. Why should we use it ?</h1>
                    <h1>💡</h1>
                </section>

                <section id="ten">
                    <ul>
                        <li>
                            <strong>1. It's like Flexbox, but better :</strong>{' '}
                            <p>
                                With Flexbox you are forced to work with
                                one-dimensional layouts, so you always need to
                                think about your layout either as rows or
                                columns at a specific time. <br />
                                With CSS Grid, you can work in a two-dimensional
                                way, which means you can work with columns and
                                rows at the same time! :)
                            </p>
                            <img src="/flex.jpg" />
                        </li>
                    </ul>
                </section>

                <section id="eleven" className="center">
                    <ul>
                        <li>
                            <strong>2. Source Order Independence</strong> :{' '}
                            <p>
                                CSS Grid allows you to make HTML into what is
                                was supposed to be. Markup of content. Not
                                visuals, which belong in the CSS.
                            </p>
                        </li>
                    </ul>
                </section>

                <section id="twelve">
                    <ul>
                        <li>
                            <strong>3. Browser Support</strong> :{' '}
                            <p>
                                In 2017, the CSS Grid Layout module was
                                published as a W3C Candidate recommendation and
                                support in all major browsers was shipped almost
                                simultaneously.
                            </p>
                            <p>
                                As of right now, <strong>90%</strong> of global
                                traffic supports this feature.
                            </p>
                            <img src="/cani.png" />
                        </li>
                    </ul>
                </section>

                <section id="thirteen">
                    <h1>3. How about AB Tasty ? 🚀</h1>
                    <img src="/turfu.png" />
                    <p style={{ fontSize: '12px' }}>turfu</p>
                </section>

                <section id="fourteen">
                    <h1>
                        Thank you !{' '}
                        <img
                            class="emoji"
                            draggable="false"
                            alt="😄"
                            src="https://twemoji.maxcdn.com/2/svg/1f604.svg"
                            data-marp-twemoji=""
                        />
                    </h1>
                </section>
            </Slideshow>
        );
    }
}
